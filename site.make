core = 7.x
api = 2

; uw_catering_theme
projects[uw_catering_theme][type] = "theme"
projects[uw_catering_theme][download][type] = "git"
projects[uw_catering_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_catering_theme.git"
projects[uw_catering_theme][download][tag] = "7.x-1.18"
projects[uw_catering_theme][subdir] = ""

; uw_catering_custom
projects[uw_catering_custom][type] = "module"
projects[uw_catering_custom][download][type] = "git"
projects[uw_catering_custom][download][url] = "https://git.uwaterloo.ca/wcms/uw_catering_custom.git"
projects[uw_catering_custom][download][tag] = "7.x-1.6"
projects[uw_catering_custom][subdir] = "custom"

; uw_catering_features
projects[uw_catering_features][type] = "module"
projects[uw_catering_features][download][type] = "git"
projects[uw_catering_features][download][url] = "https://git.uwaterloo.ca/wcms/uw_catering_features.git"
projects[uw_catering_features][download][tag] = "7.x-1.14"
projects[uw_catering_features][subdir] = "features"

; block_machine_name
projects[block_machine_name][type] = "module"
projects[block_machine_name][download][type] = "git"
projects[block_machine_name][download][url] = "https://git.uwaterloo.ca/drupal-org/block_machine_name.git"
projects[block_machine_name][download][tag] = "7.x-1.0-beta1"
projects[block_machine_name][subdir] = "contrib"

; breakpoints
projects[breakpoints][type] = "module"
projects[breakpoints][download][type] = "git"
projects[breakpoints][download][url] = "https://git.uwaterloo.ca/drupal-org/breakpoints.git"
projects[breakpoints][download][tag] = "7.x-1.6"
projects[breakpoints][subdir] = "contrib"

; copyright_block
projects[copyright_block][type] = "module"
projects[copyright_block][download][type] = "git"
projects[copyright_block][download][url] = "https://git.uwaterloo.ca/drupal-org/copyright_block.git"
projects[copyright_block][download][tag] = "7.x-2.4"
projects[copyright_block][subdir] = "contrib"

; entity_view_mode
projects[entity_view_mode][type] = "module"
projects[entity_view_mode][download][type] = "git"
projects[entity_view_mode][download][url] = "https://git.uwaterloo.ca/drupal-org/entity_view_mode.git"
projects[entity_view_mode][download][tag] = "7.x-1.0-rc1"
projects[entity_view_mode][subdir] = "contrib"

; fences
; Needs to be kept at 1.2 until issue 2605808 is fixed
projects[fences][type] = "module"
projects[fences][download][type] = "git"
projects[fences][download][url] = "https://git.uwaterloo.ca/drupal-org/fences.git"
projects[fences][download][tag] = "7.x-1.2"
projects[fences][subdir] = "contrib"

; follow
projects[follow][type] = "module"
projects[follow][download][type] = "git"
projects[follow][download][url] = "https://git.uwaterloo.ca/drupal-org/follow.git"
projects[follow][download][tag] = "7.x-2.0-alpha2"
projects[follow][subdir] = "contrib"

; field_formatter_label
; Allows rewriting field label for display
projects[field_formatter_label][type] = "module"
projects[field_formatter_label][download][type] = "git"
projects[field_formatter_label][download][url] = "https://git.uwaterloo.ca/drupal-org/field_formatter_label.git"
projects[field_formatter_label][download][tag] = "7.x-1.0-beta1"
projects[field_formatter_label][subdir] = "contrib"

; field_formatter_settings
; Dependency of field_formatter_label
projects[field_formatter_settings][type] = "module"
projects[field_formatter_settings][download][type] = "git"
projects[field_formatter_settings][download][url] = "https://git.uwaterloo.ca/drupal-org/field_formatter_settings.git"
projects[field_formatter_settings][download][tag] = "7.x-1.1"
projects[field_formatter_settings][subdir] = "contrib"

; imagefield_crop
projects[imagefield_crop][type] = "module"
projects[imagefield_crop][download][type] = "git"
projects[imagefield_crop][download][url] = "https://git.drupalcode.org/project/imagefield_crop.git"
projects[imagefield_crop][download][tag] = "7.x-2.0"
projects[imagefield_crop][subdir] = "contrib"

; imce_wysiwyg
projects[imce_wysiwyg][type] = "module"
projects[imce_wysiwyg][download][type] = "git"
projects[imce_wysiwyg][download][url] = "https://git.drupalcode.org/project/imce_wysiwyg.git"
projects[imce_wysiwyg][download][tag] = "7.x-1.0"
projects[imce_wysiwyg][subdir] = "contrib"

; linkit
projects[linkit][type] = "module"
projects[linkit][download][type] = "git"
projects[linkit][download][url] = "https://git.uwaterloo.ca/drupal-org/linkit.git"
projects[linkit][download][tag] = "7.x-3.5"
projects[linkit][subdir] = "contrib"

; onlyone
projects[onlyone][type] = "module"
projects[onlyone][download][type] = "git"
projects[onlyone][download][url] = "https://git.uwaterloo.ca/drupal-org/onlyone.git"
projects[onlyone][download][tag] = "7.x-1.14"
projects[onlyone][subdir] = "contrib"

; picture
projects[picture][type] = "module"
projects[picture][download][type] = "git"
projects[picture][download][url] = "https://git.uwaterloo.ca/drupal-org/picture.git"
projects[picture][download][tag] = "7.x-2.13"
projects[picture][subdir] = "contrib"

; rabbit_hole
projects[rabbit_hole][type] = "module"
projects[rabbit_hole][download][type] = "git"
projects[rabbit_hole][download][url] = "https://git.uwaterloo.ca/drupal-org/rabbit_hole.git"
projects[rabbit_hole][download][tag] = "7.x-2.25"
projects[rabbit_hole][subdir] = "contrib"

; viewreference
projects[viewreference][type] = "module"
projects[viewreference][download][type] = "git"
projects[viewreference][download][url] = "https://git.uwaterloo.ca/drupal-org/viewreference.git"
projects[viewreference][download][tag] = "7.x-3.5"
projects[viewreference][subdir] = "contrib"
